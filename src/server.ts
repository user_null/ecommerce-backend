/* eslint-disable @typescript-eslint/no-var-requires */
"use strict";
/*
  ## Toda la implementacion del login es de acá https://www.sitepoint.com/local-authentication-using-passport-node-js/ Y me da igual.
*/
import express from "express";
import dotenv from "dotenv";
dotenv.config();

// Ramda dependecies
import isEmpty from "ramda/src/isEmpty";
//import * as R from "ramda"

const app: express.Application = express();
const port: Readonly<string> = process.env.PORT ?? "3000";
const mongoose = require("mongoose");
const nodemailer = require("nodemailer");
const expressSession = require("express-session")({
	secret: "secret",
	resave: false,
	saveUninitialized: false
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSession);
const passport = require("passport");

app.use(passport.initialize());
app.use(passport.session());
const passportLocalMongoose = require("passport-local-mongoose");
import { Schema, model } from "mongoose";
mongoose.connect(process.env.DATABASE, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});
const UserDetail = new Schema({
	username: String,
	password: String
});

UserDetail.plugin(passportLocalMongoose);
const UserDetails = mongoose.model("userInfo", UserDetail, "userInfo");

passport.use(UserDetails.createStrategy());

passport.serializeUser(UserDetails.serializeUser());
passport.deserializeUser(UserDetails.deserializeUser());


const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
	// we're connected!
});
const ProductoSchema = new Schema({
	name: String,
	description: String,
	code: String,
	foto: String,
	precio: Number,
	stock: Number,
});
const Producto = model("Producto", ProductoSchema);

const CarritoSchema = new Schema({
	producto: {
		name: String,
		description: String,
		code: String,
		foto: String,
		precio: Number,
		stock: Number,
	},
});
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);
const Carrito = model("Carrito", CarritoSchema);
const { parse, stringify } = JSON;
const { assign } = Object;
const transform = (obj: object) => parse(stringify(assign({}, obj)));
const { find: pfind, findById: pfindID } = Producto;
const { find: cfind, findById: cfindID } = Carrito;
const { get: GET, put: PUT, delete: DEL, patch: PATCH, listen: LISTEN } = app;
const range = (Min: string) => (Max: string) =>
	!Max
		? { precio: { $gt: Min } }
		: !Min
			? { precio: { $lt: Max } }
			: { precio: { $and: [{ $gt: Min }, { $lt: Max }] } };

// Routes
const connectEnsureLogin = require("connect-ensure-login");

POST("/login", (req, res, next) => {
	passport.authenticate("local",
		(err, user, info) => {
			if (err) {
				return next(err);
			}

			if (!user) {
				return res.redirect("/login?info=" + info);
			}

			req.logIn(user, function(err) {
				if (err) {
					return next(err);
				}

				return res.redirect("/");
			});

		})(req, res, next);
});

GET("/login",
	(req, res) => {
		const transporter = nodemailer.createTransport({
			host: EMAILHOST,
			port: 465,
			secure: true,
			auth: {
				user: EMAILACCOUNTUSER,
				pass: EMAILACCOUNTPASSWORD,
			},
		});
		const message = {
			from: String(EMAILACCOUNTUSER),
			to: String(req.body?.email),
			subject: "Test Subject",
			text: "Test Text",
			html: "<h1>Cool</h1>",
		};
		transporter.sendMail(message);
		client.messages
			.create({
				from: TWILIONUMBER,
				body: "TEST",
				to: req.body?.number,
			})
			.then((message) => console.log(message.sid));

		client.messages
			.create({
				from: `whatsapp:${TWILIOPHONENUMBER}`,
				body: "Test",
				to: `whatsapp:${ADMINPHONENUMBER}`
			})
			.then(message => console.log(message.sid));
		res.sendFile("html/login.html",
			{ root: __dirname });
	});

GET("/",
	connectEnsureLogin.ensureLoggedIn(),
	(req, res) => res.sendFile("html/index.html", {root: __dirname})
);

GET("/private",
	connectEnsureLogin.ensureLoggedIn(),
	(req, res) => res.sendFile("html/private.html", {root: __dirname})
);

GET("/user",
	connectEnsureLogin.ensureLoggedIn(),
	(req, res) => res.send({user: req.user})
);
GET("/productos/listar", async ({}, res: express.Response) =>
	res.send(transform(await pfind())).sendStatus(200)
);

GET(
	"/productos/listar/:nombre",
	async ({ params: { nombre } }, res: express.Response) =>
		res.send(transform(await pfind({ name: nombre }))).sendStatus(200)
);

GET(
	"/productos/listar/:Min-:Max",
	async ({ params: { Min, Max } }, res: express.Response) =>
		res.send(transform(await pfind(range(Min)(Max)))).sendStatus(200)
);

GET("/carrito/listar", async ({}, res: express.Response) =>
	res.send(transform(await cfind())).sendStatus(200)
);

GET(
	"/productos/listar/:id",
	async ({ params: { id } }, res: express.Response) =>
		res.send(await pfindID(id)).sendStatus(200)
);

GET("/carrito/listar/:id", async ({ params: { id } }, res: express.Response) =>
	res.send(await cfindID(id)).sendStatus(200)
);

PUT("/productos/agregar", async ({ body }, res: express.Response) => {
	isEmpty(body) && res.sendStatus(400);
	const producto = new Producto(body);
	producto.save();
	res.sendStatus(200);
});

PUT("/carrito/agregar", async ({ body }, res: express.Response) => {
	isEmpty(body) && res.sendStatus(400);
	const carrito = new Carrito(body);
	carrito.save();
	res.sendStatus(200);
});

DEL(
	"/productos/borrar/:id",
	async ({ params: { id } }, res: express.Response) => {
		isEmpty(id) && res.sendStatus(400);
		Producto.findByIdAndDelete(id);
		res.sendStatus(200);
	}
);

DEL(
	"/carrito/borrar/:id",
	async ({ params: { id } }, res: express.Response) => {
		isEmpty(id) && res.sendStatus(400);
		Carrito.findByIdAndDelete(id);
		res.sendStatus(200);
	}
);

PATCH(
	"productos/actualizar/:id",
	async ({ body, params: { id } }, res: express.Response) => {
		(isEmpty(id) || isEmpty(body)) && res.sendStatus(400);
		Producto.findByIdAndUpdate(id, body);
		res.sendStatus(200);
	}
);

LISTEN(port);
